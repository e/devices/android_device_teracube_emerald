#
# Copyright (C) 2022 The LineageOS Project
#
# SPDX-License-Identifier: Apache-2.0
#

PRODUCT_MAKEFILES := \
    $(LOCAL_DIR)/lineage_emerald.mk

COMMON_LUNCH_CHOICES := \
    lineage_emerald-user \
    lineage_emerald-userdebug \
    lineage_emerald-eng