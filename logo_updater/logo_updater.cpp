/*
 * Copyright (C) 2022 ECORP SAS - Author: Jan Altensen <info@stricted.net>
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <fstream>
#include <iterator>
#include <iostream>
#include <vector>
#include <stdio.h>

using namespace std;

int main () {
    std::fstream image("/vendor/firmware/logo.bin", std::ios::binary | std::ios::in);
    if (image.fail()) {
        // bail out if logo.bin can't be opened
        return 0;
    }

    std::vector<uint8_t> image_data((std::istreambuf_iterator<char>(image)), std::istreambuf_iterator<char>());

    std::fstream partition("/dev/block/platform/bootdevice/by-name/logo", std::ios::binary | std::ios::in | std::ios::out);
    if (partition.fail()) {
        // bail out if the logo partition can't be opened
        image.close();
        return 0;
    }

    std::vector<uint8_t> partition_data((std::istreambuf_iterator<char>(partition)), std::istreambuf_iterator<char>());

    // check if the partition contents is equal to the image
    if (!std::equal(image_data.begin(), image_data.end(), partition_data.begin())) {
        // seek to the beginning
        partition.seekg(0);

        // write the logo.bin content to the partition
        partition.write((const char*)&image_data[0], image_data.size());

        // fill the rest of the partition with null bytes
        vector<char> temp(partition_data.size() - image_data.size());
        std::fill(temp.begin(), temp.begin()+(partition_data.size() - image_data.size()), 0x00);
        partition.write((const char*)&temp[0], temp.size());
    }

    // close the partitions
    image.close();
    partition.close();
    return 0;
}
